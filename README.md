2215.github.io
==============

Creating Pages with the automatic generator
The Automatic Page Generator can be used on GitHub to quickly create a web page for a project, user or organization.

User and Org Pages

To generate User and Organization pages, you'll need to create a repository named username.github.io or orgname.github.io first. The username or organization name must be your own or Pages will not build. The automatic page generator is accessible via the repository's admin page after you've created the repository. You can read more on User and Organization pages here.
